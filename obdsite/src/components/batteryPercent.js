import React from "react";
import Plot from "react-plotly.js";

const BatteryPercentage = ({ records }) => {
  return (
    <Plot
      data={[
        {
          x: records.map(x => x.BoltRecordId),
          y: records.map(y => y.BatteryLevel),
          type: "bar",
          marker: { color: "orange" }
        }
      ]}
      layout={{ width: 700, height: 500, title: "BatteryPercentage" }}
    ></Plot>
  );
};
export default BatteryPercentage;
