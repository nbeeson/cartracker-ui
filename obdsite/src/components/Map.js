import React from "react";
import Plot from "react-plotly.js";

const Map = ({ records }) => {
  let lat = records.map(x => parseFloat(x.Latitude.substring(0, 12))) || [];
  let long = records.map(x => parseFloat(x.Logitude.substring(0, 12))) || [];
  console.log(lat, long);
  return (
    <Plot
      data={[
        {
          type: "scattermapbox",
          mode: "markers",
          lon: long,
          lat: lat,
         // text: records.map(x => x.BatteryLevel)+"",
          marker: { size: 7, color: ["cyan"] }
        }
      ]}
      layout={{
        mapbox: {
            style: "stamen-terrain",
            center: { lon: -104.6994248, lat: 39.5959 },
            zoom: 12
        },
        width: 800,
        height: 600,
        title: "Map",

      }}
    ></Plot>
  );
};
export default Map;
