import React from "react";
import Plot from "react-plotly.js";

const GraphTest = ({ records }) => {
  return (
    <Plot
      data={[
        {
          x: records.map(x => x.BoltRecordId),
          y: records.map(x => x.WattHourPerMile),
          type: "scatter",
          mode: "lines+markers",
          marker: { color: "red" }
        }
      ]}
      layout={{ width: 700, height: 500, title: "Watt Hours Per Mile" }}
    ></Plot>
  );
};
export default GraphTest;
