import React from "react";

const Records = ({ records }) => {
    var total=0;
    records.forEach(e => {
        total+=parseFloat(e.WattHourPerMile)
    })
    console.log(total)
  return (
    <div>
      <center>
        <h1>Bolt Stats</h1>
      </center>
      <div class="card">
        <div class="card-body">
          <h5 class="card-title">Average Watt Hours Per Mile</h5>
          <p class="card-text">{
          total/(records.length || 1)
          }</p>
        </div>
      </div>
      {records.map(record => (
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">{record.BoltRecordId}</h5>
            <h6 class="card-subtitle mb-2 text-muted">
              {record.WattHourPerMile}
            </h6>
            <p class="card-text">{record.Logitude}</p>
          </div>
        </div>
      ))}
    </div>
  );
};

export default Records;
