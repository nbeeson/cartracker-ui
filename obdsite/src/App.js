import React, { Component } from "react";
import Records from "./components/records";
import GraphTest from "./components/graphTest";
import BatteryPercent from "./components/batteryPercent";
import Map from "./components/Map";

class App extends Component {
  state = {
    records: [],
    startDate: "2020-01-01"
  };
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.ApiCall = this.ApiCall.bind(this);
  }

  handleChange(event) {
    this.setState({ startDate: event.target.value });
  }

  handleSubmit(event) {
    console.log("A date was submitted: " + this.state.startDate);
    event.preventDefault();
    this.ApiCall();
  }

  componentDidMount() {
    this.ApiCall();
  }
  ApiCall() {
    fetch(
      `http://localhost:3007/getallbydate?startDate=${this.state.startDate}`
    )
      .then(res => res.json())
      .then(data => {
        this.setState({ records: data });
      })
      .catch(console.log);
  }

  render() {
    return (
      <div>
        <Map records={this.state.records} style={`backgroundColor: #0f111a`} />
        <GraphTest records={this.state.records} />
        <BatteryPercent records={this.state.records} />
        <form onSubmit={this.handleSubmit}>
          <label>
            <p>
            Start Date 

            </p>
          </label>
          <input
            type="text"
            value={this.state.startDate}
            onChange={this.handleChange}
          />
          <input type="submit" value="Submit" />
        </form>
        <Records records={this.state.records} />
      </div>
    );
  }
}

export default App;
